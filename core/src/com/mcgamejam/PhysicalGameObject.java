package com.mcgamejam;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;

public abstract class PhysicalGameObject {
	protected boolean isSelected;
	protected Vector2 position = new Vector2();
	protected Vector2 dimensions = new Vector2();
	
	private Texture whatever = new Texture("badlogic.jpg");
	private ShapeRenderer shape = new ShapeRenderer();
	
	public abstract void initializePhysics(World physicsWorld);
	
	private Vector2 touchPosition = new Vector2();
	
	public void update(GameState gameState)
	{
		touchPosition.set(Gdx.input.getX(), gameState.getDimensions().y - Gdx.input.getY());
		if (Gdx.input.justTouched())
		{
			if (touchPosition.x > position.x && touchPosition.y > position.y && touchPosition.x < dimensions.x + position.x && touchPosition.y < dimensions.y + position.y)
			{
				ArrayList<PhysicalGameObject> otherObjects = gameState.getSelectablePhysicalGameObjects();
				for (int i = 0; i < otherObjects.size(); i++)
				{
					otherObjects.get(i).isSelected = false;
				}
				isSelected = true;
			}
		}
	}
	
	public void render(SpriteBatch batch)
	{
		if (isSelected)
		{
			batch.end();
			Gdx.gl.glEnable(GL30.GL_BLEND);
		    Gdx.gl.glBlendFunc(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA);
			shape.begin(ShapeType.Line);
			shape.setColor(1f, 0f, 0f, 0.5f);
			shape.circle(position.x + dimensions.x / 2, position.y + dimensions.y / 2, 30);
			shape.end();
			Gdx.gl.glDisable(GL30.GL_BLEND);
			batch.begin();
		}
	}
}
