package com.mcgamejam;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class SpikeRobot extends Robot {
	protected Texture texture;
	protected TextureRegion textureRegion;
	private static int NUM_FRAMES = 8;
	
	protected SpikeRobot(float x, float y, boolean faceRight) {
		super(x, y);
		facingRight = faceRight;
		texture = new Texture("elecwalk.png");
		textureRegion = new TextureRegion(texture);
		dimensions = new Vector2(64, 64);
	}
	
	@Override
	public void update(GameState gameState)
	{
		counter++;
		if (counter == frameChangeFrequency)
		{
			frame++;
			if (frame == NUM_FRAMES)
			{
				frame = 0;
			}
			textureRegion.setRegion(frame * 204, 0, 204, 227);
			counter = 0;
		}
		super.update(gameState);
		
		// logic stuff
		if (isSelected)
		{
			if (Gdx.input.justTouched())
			{
				float xDir = Gdx.input.getX() - position.x - dimensions.x / 2;
				body.applyForceToCenter(new Vector2(xDir, 1), true);
			}
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(textureRegion, position.x, position.y, dimensions.x, dimensions.y);
		super.render(batch);
	}
}
