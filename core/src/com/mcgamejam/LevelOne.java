package com.mcgamejam;

import com.badlogic.gdx.math.Vector2;

public class LevelOne extends Level {
	public LevelOne() {
		stairbot = new StaircaseRobot(800, 600, true);
		spikebot = new SpikeRobot(400, 350, true);
		
		Wall tester = new Wall("Platform.png", new Vector2(200, 300), 5, 880);
		Wall tester2 = new Wall("Platform.png", new Vector2(1000, 500), 5, 40);
		Wall tester3 = new Wall("Platform.png", new Vector2(800, 450), 5, 200);
		walls.add(tester);
		walls.add(tester2);
		walls.add(tester3);
		
		lights.add(new Light(300, 300, 1, 0));
		Obstacle test = new Obstacle("wire.png", new Vector2(1000, 300), 10, 20);
		obstacles.add(test);

		exit = new Exit("wire.png", new Vector2(200, 300), 30, 30);
	}
}
