package com.mcgamejam;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class StaircaseRobot extends Robot{
	protected Vector2 dy, dx;
	protected boolean facingRight, isStairs;
	protected Texture robotTexture;
	protected TextureRegion robotTextureRegion;
	private int robotNumFrames = 8;
	protected Texture stairTexture;
	protected Fixture stairFixture;
	protected float stairTimeStart;
	
	protected StaircaseRobot(float x, float y, boolean faceRight) {
		super(x, y);

		//right
		if(facingRight) {
			if((x + 150) < 1280) {
				dx = new Vector2(x + 150, y);
			}
			else {
				dx = new Vector2(1280, y);
			}
		}
		else {											//left
			if((x - 150) > 0) {
				 dx = new Vector2(x - 150, y);
			}
			else {
				dx = new Vector2(0, y);
			}
		}
		
		if((y + 100) < 720) {
			dy = new Vector2(x, y + 100);
		}
		else {
			dy = new Vector2(x, 720);
		}
		
		robotTexture = new Texture("treadright.png");
		stairTexture = new Texture("treadstair.png");
		stairTimeStart = 0;
		facingRight = faceRight;
		inLight = false;
		robotTextureRegion = new TextureRegion(robotTexture);
		dimensions = new Vector2(64, 64);
	}
	
	@Override
	public void update(GameState gameState) {
		if(!isStairs && inLight) {
			ability();
			stairTimeStart = gameState.getGameTime();
		}
		else if((gameState.getGameTime() - stairTimeStart) >= 1 && isStairs) {
			changeBack();
		}
		
		//animation stuff
		counter++;
		if (counter == frameChangeFrequency)
		{
			frame++;
			if (frame == robotNumFrames)
			{
				frame = 0;
			}
			if (isStairs)
			{
				robotTextureRegion.setRegion(frame * 280, 0, 280, 462);
			}
			else
			{
				robotTextureRegion.setRegion(frame * 200, 0, 200, 200);
			}
			counter = 0;
		}
		super.update(gameState);
		
		// logic stuff
		if (isSelected)
		{
			if (Gdx.input.justTouched())
			{
				float xDir = Gdx.input.getX() - position.x - dimensions.x / 2;
				body.applyForceToCenter(new Vector2(xDir, 1), true);
			}
		}
	}

	public void ability() {
		FixtureDef fixtureDef = new FixtureDef();
		Vector2[] vertices = new Vector2[3];
		vertices[0] = new Vector2(0, 0);
		vertices[1] = new Vector2(64 * GameState.PHYSICS_SCALE, 0);
		vertices[2] = new Vector2(0, 128 * GameState.PHYSICS_SCALE);
		PolygonShape shape = new PolygonShape();
		shape.set(vertices);
		fixtureDef.shape = shape;
		fixtureDef.density = 1000;
		fixtureDef.friction = 100;
		stairFixture = body.createFixture(fixtureDef);
		isStairs = true;
		robotNumFrames = 7;
		robotTextureRegion = new TextureRegion(stairTexture);
	}
	
	public void changeBack() {
		body.destroyFixture(stairFixture);
		isStairs = false;
		robotNumFrames = 8;
		robotTextureRegion = new TextureRegion(robotTexture);
	}
	
	public void directionSwap() {
		if(facingRight) {
			if((position.x - 150) > 0) {
				 dx = new Vector2(position.x - 150, position.y);
			}
			else {
				dx = new Vector2(0, position.y);
			}
			facingRight = false;
		}
		else {
			if((position.x + 150) < 1280) {
				dx = new Vector2(position.x + 150, position.y);
			}
			else {
				dx = new Vector2(1280, position.y);
			}
			facingRight = true;
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		if (isStairs)
		{
			batch.draw(stairTexture, position.x, position.y, dimensions.x, 2 * dimensions.y);
		}
		else
		{
			batch.draw(robotTextureRegion, position.x, position.y, dimensions.x, dimensions.y);
		}
		super.render(batch);
	}
}
